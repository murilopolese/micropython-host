try:
    import usocket as socket
except:
    import socket

CONTENT = b"""\
HTTP/1.0 200 OK

<html>
<head>
    <title>There you go!</title>
</head>
<body>
    <h1>There you go!</h1>
</body>
</html>
"""

listen_s = None
client_s = None

def accept_handler(listen_sock):
    res = listen_sock.accept()
    client_sock = res[0]
    client_addr = res[1]

    client_stream = client_sock
    client_stream.setblocking(False)

    req = client_stream.readline()
    while True:
        h = client_stream.readline()
        if h == b"" or h == b"\r\n":
            break

    CONTENT = b"""\
HTTP/1.0 200 OK

    """
    client_stream.write(CONTENT)
    f = open('webrepl.html', 'r')
    for line in f:
        client_stream.write(line)

    client_stream.close()
    client_sock.close()

def main():
    s = socket.socket()
    ai = socket.getaddrinfo("0.0.0.0", 80)
    addr = ai[0][-1]
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)
    s.listen(1)
    s.setsockopt(socket.SOL_SOCKET, 20, accept_handler)

main()
