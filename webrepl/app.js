let fakeContent = `from time import sleep

print("Hello world!")
print(123)

while True:
    print("ping")
    sleep(1)
`

let fakeConsole = [
    '>> print("Hello world")',
    'Hello world',
    '>> print(123)',
    'Hello world',
    '>> print(123)',
    'Hello world',
    '>> print(123)',
    'Hello world',
    '>> print(123)',
    'Hello world',
    '>> print(123)',
    '123'
];

let fakeTree = [
    {
        fileName: 'www', type: 'folder', files:
        [
            { fileName: 'webrepl.html', type: 'file' },
            { fileName: 'remote.html', type: 'file' },
            { fileName: 'synth.html', type: 'file' }
        ]
    },
    { fileName: 'boot.py', type: 'file' },
    { fileName: 'main.py', type: 'file' },
    { fileName: 'webrepl_cfg.py', type: 'file' }
];

// =============================================================================


/**
* Initial app state
*/
let appInitialState = {
    address: 'ws://localhost:8266',
    ws: null,
    loading: false,
    loadingMessage: '',
    error: false,
    errorMessage: '',
    toolbar: [
        {label: 'Run', emit: 'run', disabled: false},
        {label: 'Stop', emit: 'stop', disabled: false},
        {label: 'Save', emit: 'save', disabled: false},
        {label: 'Delete', emit: 'delete', disabled: false},
        {label: 'Refresh', emit: 'refresh', disabled: false}
    ],
    selectedFile: {
        fileName: '',
        content: ''
    },
    console: [],
    connected: false,
    files: []
}

/**
* Handles "tab" on textarea
*/
let textControl = (event) => {
    let element = event.target;
    if(event.keyCode==9 || event.which==9) {
        event.preventDefault();
        var s = element.selectionStart;
        element.value = element.value.substring(0,element.selectionStart) + "\t" + element.value.substring(element.selectionEnd);
        element.selectionEnd = s+1;
    }
};

/**
* Scrolls console container down
*/
let scrollDown = () => {
    let consoleContainer = document.querySelector('#console'),
        logs = document.querySelector('#console pre');
    consoleContainer.scrollTo(0, logs.offsetHeight);
}

/**
* Loads app framework and render methods
*/
let html = require('choo/html'),
	raw = require('choo/html/raw'),
    choo = require('choo'),
    app = choo();


/**
* Define app store and
*/
let appStore = (state, emitter) => {
    state.appState = appInitialState;
    let appState = state.appState;

    /**
    * Connects to board
    */
    emitter.on('connectToBoard', () => {
        let onMessage = (e) => {
            console.log(e);
            emitter.emit('log', e);
        }
        let onOpen = (e) => {
            appState.ws.onmessage = onMessage.bind(this);
            emitter.emit('loadTree');
            emitter.emit('loadFile', state.params.filename);
        }
        let onError = (e) => {
            emitter.emit('error', 'Could not connect to board');
        }
        let onClose = (e) => {
            if (!appState.error) {
                emitter.emit('error', 'Connection interrupted');
            }
        }
        emitter.emit('loading', 'Connecting...');
        appState.ws = new WebSocket(appState.address);
        appState.ws.onopen = onOpen.bind(this);
        appState.ws.onclose = onClose.bind(this);
        appState.ws.onerror = onError.bind(this);
    });

    /**
    * Gets the file content from the board
    */
    emitter.on('loadFile', (fileName) => {
        if (fileName) {
            emitter.emit('loading', 'Loading file...');
            //emitter.emit('fileLoaded', {fileName, content});
        }
    });
    /**
    * Gets the file tree from the board
    */
    emitter.on('loadTree', () => {
        emitter.emit('loading', 'Loading tree...');
        appState.files = fakeTree;
    });

    emitter.on('run', () => console.log('run'));
    emitter.on('stop', () => console.log('stop'));
    emitter.on('save', () => console.log('save'));
    emitter.on('delete', () => console.log('delete'));

    /**
    * On App load, connect to board, get file tree and load any file if it
    * has a `filename` parameter on the url.
    */
    emitter.on('DOMContentLoaded', () => {
        emitter.emit('connectToBoard');
    });

    emitter.on('loading', (loadingMessage) => {
        appState.error = false;
        appState.errorMessage = '';
        appState.loading = true;
        appState.loadingMessage = loadingMessage;
        emitter.emit('render');
    });

    emitter.on('fileLoaded', (file) => {
        appState.selectedFile.fileName = file.fileName;
        appState.selectedFile.content = file.content;
        appState.loading = false;
        appState.loadingMessage = '';
        emitter.emit('render');
    });

    emitter.on('treeLoaded', (files) => {
        appState.files = files;
        appState.loading = false;
        appState.loadingMessage = '';
        emitter.emit('render');
    });

    emitter.on('log', (log) => {
        appState.console.push(log);
        emitter.emit('render');
        scrollDown();
    });

    emitter.on('error', (message) => {
        appState.error = true;
        appState.errorMessage = message;
        emitter.emit('render');
    });
};

/**
* View with main controls for the app
*/
let toolbarView = (state, emit) => {
    return state.map((btnData) => {
        let onClick = (e) => {
            emit(btnData.emit);
        }
        let disabled = btnData.disabled ? 'disabled' : '';
        return html`<button class="btn reset" ${disabled} onclick=${onClick}>${btnData.label}</button>`;
    })
};

/**
* Tree view with files from the board
*/
let treeView = (state, emit) => {
    /**
    * Recursive function that renders the file tree
    */
    let renderTree = (file) => {
        let fileName = file.fileName;
        if (file.type === 'folder') {
            return html`
                <li>
                    <a>${fileName}/</a>
                    <ul>
                        ${file.files.map(renderTree)}
                    </ul>
                </li>
            `;
        } else {
            /**
            * Handles click on link.
            */
            let onClick = (e) => {
                e.preventDefault();
                emit('loading', 'Loading file...');
                getFile(fileName)
                    .then((content) => {
                        emit('pushState', `#${fileName}`);
                        emit('fileLoaded', {fileName, content})
                    })
                    .catch((error) => {
                        emit('error', error);
                    });
                return false;
            }
            return html`
            <li>
                <a onclick=${onClick} href="#${file.fileName}">
                    ${file.fileName}
                </a>
            </li>
            `
        }
    }
    return state.map(renderTree);
}

/**
* View with the app status
*/
let statusView = (state, emit) => {
    let className = '',
        message = '';
    if (state.error) {
        className = 'pink';
        message = state.errorMessage;
    } else if (state.loading) {
        className = 'orange';
        message = state.loadingMessage;
    } else {
        return html``;
    }

    return html`
        <span class="${className}">${message}</span>
    `;
}

/**
* Render console/terminal lines
*/
let renderConsole = (state) => {
	return html`<div>${state}</div>`;
}

/**
* Main app view
*/
let mainView = (state, emit) => {
    return html`
    <div id="app">
        <div id="sidebar">
            <div id="toolbar">
                ${toolbarView(state.appState.toolbar, emit)}
            </div>
            <div id="tree">
                ${treeView(state.appState.files, emit)}
            </div>
            <div id="status">
                ${statusView(state.appState, emit)}
            </div>
        </div>
        <div id="content">
            <div class="filename">
                <input
                    class="reset input"
                    type="text"
                    placeholder="filename.py"
                    value="${state.appState.selectedFile.fileName}">
            </div>
            <div id="code">
                <textarea
                    id="editor"
                    class="reset input"
                    onkeydown=${textControl}>${raw(state.appState.selectedFile.content)}</textarea>
            </div>
            <div class="label">console</div>
            <div id="console" class="input">
                <pre>${state.appState.console.map(renderConsole)}</pre>
            </div>
            <div class="console-input">
                <input class="reset input" type="text" placeholder="print('hello world')">
            </div>
        </div>
    </div>
    `
};

app.use(appStore);
app.route('/', mainView);
app.route('/:filename', mainView);
app.mount('#app');
