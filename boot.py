import network
from time import sleep
import webrepl

ap = network.WLAN(network.AP_IF)
sta = network.WLAN(network.STA_IF)

ap.active(True)
sta.active(False)

ap.config(essid="Fabulous Wifi", password="fabulous", authmode=4) #authmode=1 == no pass

webrepl.start()

